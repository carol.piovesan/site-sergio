<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Destaques extends Model
{
    protected $table    = 'destaques';
    protected $fillable = [
        'imagem_1',
        'titulo_1',
        'subtitulo_1',
        'texto_1',
        'slug_1',

        'imagem_2',
        'titulo_2',
        'subtitulo_2',
        'texto_2',
        'slug_2',

        'imagem_3',
        'titulo_3',
        'subtitulo_3',
        'texto_3',
        'slug_3',

        'imagem_4',
        'titulo_4',
        'subtitulo_4',
        'texto_4',
        'slug_4',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug_1 =  Str::slug($model->titulo_1, '-');
            $model->slug_2 =  Str::slug($model->titulo_2, '-');
            $model->slug_3 =  Str::slug($model->titulo_3, '-');
            $model->slug_4 =  Str::slug($model->titulo_4, '-');

            return true;
        });
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value) {
            $data = str_replace('/', '-', $value);
            return date('d/m/Y H:i:s', strtotime($data));
        }
    }

    public function getImagemn1Attribute()
    {
        if ($this->imagem_1 != null) {
            return asset('imagem/thumbnail/' . $this->imagem_1);
        } else {
            return asset('imagem/site/default-image.jpg');
        }
    }

    public function getImagemn2Attribute()
    {
        if ($this->imagem_2 != null) {
            return asset('imagem/thumbnail/' . $this->imagem_2);
        } else {
            return asset('imagem/site/default-image.jpg');
        }
    }

    public function getImagemn3Attribute()
    {
        if ($this->imagem_3 != null) {
            return asset('imagem/thumbnail/' . $this->imagem_3);
        } else {
            return asset('imagem/site/default-image.jpg');
        }
    }

    public function getImagemn4Attribute()
    {
        if ($this->imagem_4 != null) {
            return asset('imagem/thumbnail/' . $this->imagem_4);
        } else {
            return asset('imagem/site/default-image.jpg');
        }
    }
}
