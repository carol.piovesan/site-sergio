<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidades extends Model
{

	protected $table    = 'paginas';
	protected $fillable = [
		'pagina',
		'texto'
	];

}
