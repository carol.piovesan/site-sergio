<?php

namespace App\Http\Controllers;

use App\Destaques;
use App\Http\Requests\DestaquesRequest;
use DB;
use Image;

class DestaquesController extends Controller
{
    private $view = 'destaques.';

    public function create()
    {
        $registro = Destaques::first();
       // dd($registro);
       // dd($registro->imagem_1);
        
        return view($this->view.'criar_editar', compact('registro'));
    }

    public function store(DestaquesRequest $request)
    {
        $registro = Destaques::first();
       // dd($registro);
        //dd($request->all());
        try {
            DB::beginTransaction();

            if(!isset($registro)){
                Destaques::create($request->all());
            }else{

                if($request->hasFile('arquivo_1')){
                    $image = $request->file('arquivo_1');
                    $request['imagem_1'] = time().'1.'.$image->extension();
    
                    if (!file_exists('imagem/thumbnail')) {
                        mkdir('imagem/thumbnail', 0777, true);
                    }
    
                    $destinationPath = public_path('imagem/thumbnail');
    
                    $img = Image::make($request->image1base64)->encode($image->getClientOriginalExtension());
    
                    $img->save($destinationPath.'/'.$request['imagem_1']);
    
                    $destinationPath = public_path('/imagem');
                    $image->move($destinationPath, $request['imagem_1']);
                }

                if($request->hasFile('arquivo_2')){
                    $image = $request->file('arquivo_2');
                    $request['imagem_2'] = time().'2.'.$image->extension();
    
                    if (!file_exists('imagem/thumbnail')) {
                        mkdir('imagem/thumbnail', 0777, true);
                    }
    
                    $destinationPath = public_path('imagem/thumbnail');
    
                    $img = Image::make($request->image2base64)->encode($image->getClientOriginalExtension());
    
                    $img->save($destinationPath.'/'.$request['imagem_2']);
    
                    $destinationPath = public_path('/imagem');
                    $image->move($destinationPath, $request['imagem_2']);
                }

                if($request->hasFile('arquivo_3')){
                    $image = $request->file('arquivo_3');
                    $request['imagem_3'] = time().'3.'.$image->extension();
    
                    if (!file_exists('imagem/thumbnail')) {
                        mkdir('imagem/thumbnail', 0777, true);
                    }
    
                    $destinationPath = public_path('imagem/thumbnail');
    
                    $img = Image::make($request->image3base64)->encode($image->getClientOriginalExtension());
    
                    $img->save($destinationPath.'/'.$request['imagem_3']);
    
                    $destinationPath = public_path('/imagem');
                    $image->move($destinationPath, $request['imagem_3']);
                }

                if($request->hasFile('arquivo_4')){
                    $image = $request->file('arquivo_4');
                    $request['imagem_4'] = time().'4.'.$image->extension();
    
                    if (!file_exists('imagem/thumbnail')) {
                        mkdir('imagem/thumbnail', 0777, true);
                    }
    
                    $destinationPath = public_path('imagem/thumbnail');
    
                    $img = Image::make($request->image4base64)->encode($image->getClientOriginalExtension());
    
                    $img->save($destinationPath.'/'.$request['imagem_4']);
    
                    $destinationPath = public_path('/imagem');
                    $image->move($destinationPath, $request['imagem_4']);
                }
//dd($request->all());
                $registro->update($request->all());
            }

            //dd($registro);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect()->back()->with('error','Não foi possível cadastrar!');
        }            

        DB::commit();

        return redirect()->back()->with('notice', 'Registro salvo com sucesso!');
    }


}