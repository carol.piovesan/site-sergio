<?php

namespace App\Http\Controllers;

use App\Especialidades;
use App\Http\Requests\EspecialidadesRequest;
use Illuminate\Support\Facades\DB;

class EspecialidadesController extends Controller
{
    private $view = 'especialidades.';

    public function index()
    { 
        $registros = Especialidades::orderBy('descricao')->get();

        return view($this->view.'listar', compact('registros'));
    }

    public function create()
    {
        $registro = Especialidades::where('pagina','especialidades')->first();
        
        return view($this->view.'criar_editar', compact('registro'));
    }

    public function store(EspecialidadesRequest $request)
    {
        $request['pagina'] = 'especialidades';
        try {
            DB::beginTransaction();

            if($request->id == 0){
                Especialidades::create($request->all());
            }else{
                Especialidades::find($request->id)->update($request->all());
            }
            
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            return redirect()->back()->with('error','Não foi possível cadastrar!');
        }            

        DB::commit();

        return redirect()->back()->with('notice', 'Registro salvo com sucesso!');
    }


}