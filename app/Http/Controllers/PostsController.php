<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests\PostsRequest;
use DB;
use Image;
use Storage;


class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $view = 'posts/';

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $registros = Post::orderBy('id','desc')
        ->get();
        //dd($registros);

        return view($this->view.'index', compact('registros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->view.'criar_editar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
        //dd($request->all());
        try {
            DB::beginTransaction();

            if($request->hasFile('arquivo_principal')){
                $image = $request->file('arquivo_principal');
                $request['capa'] = time().'.'.$image->extension();

                if (!file_exists('imagem/thumbnail')) {
                    mkdir('imagem/thumbnail', 0777, true);
                }

                $destinationPath = public_path('imagem/thumbnail');

                $img = Image::make($request->imagebase64)->encode($image->getClientOriginalExtension());


                $img->save($destinationPath.'/'.$request['capa']);


                $destinationPath = public_path('/imagem');
                $image->move($destinationPath, $request['capa']);
            }


            Post::create($request->all());
        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollBack();

            return redirect()->route('posts.index')->with('error','Não foi possível cadastrar!');
        }

        DB::commit();

        return redirect()->route('posts.index')->with('notice', 'Registro cadastrado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registro = Post::find($id);
        
        return view($this->view.'criar_editar', compact('registro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $post =  Post::find($id);

            DB::beginTransaction();

            if($request->hasFile('arquivo_principal')){
                $image = $request->file('arquivo_principal');

                if($post->capa != null){
                    unlink("imagem/".$post->capa);
                    unlink("imagem/thumbnail/".$post->capa);
                }

                $request['capa'] = time().'.'.$image->extension();

                if (!file_exists('imagem/thumbnail')) {
                    mkdir('imagem/thumbnail', 0777, true);
                }

                $destinationPath = public_path('imagem/thumbnail');
                $img = Image::make($request->imagebase64)->encode($image->getClientOriginalExtension());


                $img->save($destinationPath.'/'.$request['capa']);

                $destinationPath = public_path('/imagem');
                $image->move($destinationPath, $request['capa']);
            }

            $post->update($request->all());
        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollBack();

            return redirect()->route('posts.index')->with('error','Não foi possível alterar!');
        }

        DB::commit();

        return redirect()->route('posts.index')->with('notice', 'Registro alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $post =  Post::find($id);

            if($post->capa != null){
                unlink("imagem/".$post->capa);
                unlink("imagem/thumbnail/".$post->capa);
            }

            $post->delete();

        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error','Não foi possível excluir!');
        }

        DB::commit();
        return redirect()->route('posts.index')->with('notice', 'Registro excluído com sucesso!');
    }
}
