<?php

namespace App\Http\Controllers;

use App\Destaques;
use App\Especialidades;
use Illuminate\Http\Request;
use App\Post;
use App\Mail\Contato;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{

    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->take(3)->get();
        
        return view("site/index", compact('posts'));
    }

    public function pagina($destaque_numero, $pagina)
    {
        $destaque = Destaques::first();

        switch ($destaque_numero) {
            case '1':
                $imagem = $destaque->getImagemn1Attribute();
                $titulo = $destaque->titulo_1;
                $subtitulo = $destaque->subtitulo_1;
                $texto = $destaque->texto_1;
                break;

            case '2':
                $imagem = $destaque->getImagemn2Attribute();
                $titulo = $destaque->titulo_2;
                $subtitulo = $destaque->subtitulo_2;
                $texto = $destaque->texto_2;
                break;

            case '3':
                $imagem = $destaque->getImagemn3Attribute();
                $titulo = $destaque->titulo_3;
                $subtitulo = $destaque->subtitulo_3;
                $texto = $destaque->texto_3;
                break;

            case '4':
                $imagem = $destaque->getImagemn4Attribute();
                $titulo = $destaque->titulo_4;
                $subtitulo = $destaque->subtitulo_4;
                $texto = $destaque->texto_4;
                break;
        }

        return view("site/destaque", compact('imagem', 'titulo','subtitulo','texto'));
    }

    public function sobre()
    {
        return view("site/sobre");
    }

    public function especialidades()
    {
        $especialidades = Especialidades::where('pagina', 'especialidades')->first();

        return view("site/especialidades", compact('especialidades'));
    }

    public function profissionaisParceiros()
    {
        return view("site/profissionais-parceiros");
    }


    public function posts()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(9);

        return view("site/posts", compact('posts'));
    }

    public function postagem($slug)
    {

        try {
            $post = Post::whereSlug($slug)->first();

            if ($post) {
                return view("site/postagem", compact('post'));
            } else {
                //return view('siteTema::erro');
            }
        } catch (QueryException $e) {
            // return view('siteTema::erro');
        }
    }

    public function contato()
    {
        return view("site/contato");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enviarEmail(Request $request)
    {
        $secret = "6Lfe8cEZAAAAAMf-XbCqLvckjMmeHvXwxCSGcrFK";

        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $request->captcha);
        $responseData = json_decode($verifyResponse);

        if ($responseData->success) {
            Mail::to("serd.jr@gmail.com")->send(new Contato($request->nome, $request->email, $request->assunto, $request->mensagem));

            if (Mail::failures()) {
                return redirect()->back()->with('error', 'Não foi possível enviar a mensagem! Tente novamente!');
            } else {
                return redirect()->back()->with('success', 'Mensagem enviada com sucesso!');
            }
        } else {
            return redirect()->back()->with('error', 'Não foi possível enviar sua mensagem!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
