<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DestaquesRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo_1'                 => 'required|max:255,titulo_1,',
            'subtitulo_1'              => 'nullable|max:255',
            'texto_1'                  => 'required',
            'titulo_2'                 => 'required|max:255,titulo_2,',
            'subtitulo_2'              => 'nullable|max:255',
            'texto_2'                  => 'required',
            'titulo_3'                 => 'required|max:455,titulo_3,',
            'subtitulo_3'              => 'nullable|max:455',
            'texto_3'                  => 'required',
            'titulo_4'                 => 'required|max:455,titulo_4,',
            'subtitulo_4'              => 'nullable|max:455',
            'texto_4'                  => 'required',
        ];
    }
}
