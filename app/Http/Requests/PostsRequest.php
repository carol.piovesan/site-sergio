<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'                 => 'required|max:255,titulo,',
            'subtitulo'              => 'nullable|max:255',
            'texto'                  => 'required',
            'data_hora'              => 'required|date_format:d/m/Y'
        ];
    }
}
