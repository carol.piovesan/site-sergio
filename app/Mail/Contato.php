<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contato extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $email;
    public $assunto;
    public $mensagem;


    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($nome, $email, $assunto,$mensagem)
    {
        $this->nome         = $nome;
        $this->email        = $email;
        $this->assunto     = $assunto;
        $this->mensagem     = $mensagem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->email, $this->nome)
        ->subject('Contato via Website')
        ->view('site/email');
    }
}
