<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;
use Carbon\Carbon;

class Post extends Model
{

	protected $table    = 'posts';
	protected $fillable = [
		'capa',
		'data_hora',
		'titulo',
		'subtitulo',
		'texto',
		'slug',
		'visualizacoes'
	];

	public static function boot()
	{
		parent::boot();

		static::saving(function($model)
		{
        	//dd(Str::slug($model->titulo, '-'));
			$model->slug =  Str::slug($model->titulo, '-');;
			return true;
		});
	}


	public function getDataHoraAttribute($value)
	{
		if($value){
			$data = str_replace('/', '-', $value);
			return date('d/m/Y', strtotime($data));

		}
	}

	public function setDataHoraAttribute($value)
	{
		if($value){
			$data = str_replace('/', '-', $value);

			$this->attributes['data_hora'] =  date('Y-m-d', strtotime($data));
		}
	}

	public function getCreatedAtAttribute($value)
	{
		if($value){
			$data = str_replace('/', '-', $value);
			return date('d/m/Y H:i:s', strtotime($data));
		}
	} 

	public function getDataHoraExtensoAttribute()
	{
		setlocale(LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
		
		
		return Carbon::parse(date('Y-m-d', strtotime($this->attributes['data_hora'])))->formatLocalized('%d de %B de %Y');
	}

	public function getImagemAttribute()
	{
		if($this->capa != null){
			return asset('imagem/thumbnail/'.$this->capa);
		}else{
			return asset('imagem/site/default-image.jpg');
		}
	}

	public function getParteDescricaoAttribute(){
		if($this->texto)
			
			return mb_substr(strip_tags(html_entity_decode($this->texto)), 0, 300, "utf-8" ) . ' ...';
		else
			return null;
	}	

	public function getParteDescricaoMaiorAttribute(){
		if($this->texto)
			return mb_substr(strip_tags(html_entity_decode($this->texto)), 0, 550, "utf-8" ) . ' ...';
		else
			return null;
	}

}
