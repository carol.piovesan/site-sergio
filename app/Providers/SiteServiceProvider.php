<?php

namespace App\Providers;

use App\Destaques;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\View;

class SiteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer('*', function ($view) {
            $destaques = Destaques::first();

            $view->with(compact('destaques'));
        });
    }

    public function register()
    {
    }
}
