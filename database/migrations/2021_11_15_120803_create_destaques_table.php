<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1')->nullable();
            $table->string('titulo_1');
            $table->string('subtitulo_1')->nullable();
            $table->text('texto_1');
            $table->string('slug_1');

            $table->string('imagem_2')->nullable();
            $table->string('titulo_2');
            $table->string('subtitulo_2')->nullable();
            $table->text('texto_2');
            $table->string('slug_2');

            $table->string('imagem_3')->nullable();
            $table->string('titulo_3');
            $table->string('subtitulo_3')->nullable();
            $table->text('texto_3');
            $table->string('slug_3');


            $table->string('imagem_4')->nullable();
            $table->string('titulo_4');
            $table->string('subtitulo_4')->nullable();
            $table->text('texto_4');
            $table->string('slug_4');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destaques');
    }
}
