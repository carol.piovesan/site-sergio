<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 DB::table('users')->delete();

        User::create([
			'name'        	 => 'Sergio Duarte Junior',
			'email'       	 => 'sergioduartejunior@yahoo.com.br',
			'password'    	 =>  Hash::make('senhadoinho123')
			]);

         User::create([
			'name'        	 => 'Caroline Piovesan de Moraes',
			'email'       	 => 'carol.piovesanm@gmail.com',
			'password'    	 =>  Hash::make('senhacarol123')
		
			]);

    }
}
