/*
Name: 			View - Contact
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version:	6.1.0
*/

(function($) {

	'use strict';

	/*
	Custom Rules
	*/
	
	// No White Space
	$.validator.addMethod("noSpace", function(value, element) {
		return value.search(/[a-z0-9]/i) == 0;
	}, 'Please fill this required field.');

	/*
	Assign Custom Rules on Fields
	*/
	$.validator.addClassRules({
		'form-control': {
			noSpace: true
		}
	});

	/*
	Contact Form: Basic
	*/
	$('#contactForm').validate();

	/*
	Contact Form: Advanced
	*/
	$('#contactFormAdvanced').validate({
		onkeyup: false,
		onclick: false,
		onfocusout: false,
		rules: {
			'captcha': {
				captcha: true
			},
			'checkboxes[]': {
				required: true
			},
			'radios': {
				required: true
			}
		},
		errorPlacement: function(error, element) {
			if (element.attr('type') == 'radio' || element.attr('type') == 'checkbox') {
				error.appendTo(element.closest('.form-group'));
			} else {
				error.insertAfter(element);
			}
		}
	});

}).apply(this, [jQuery]);