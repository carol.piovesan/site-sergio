@extends('layout.base')

@section('title', 'Destaques')

@section('content_header')
    <h1>Destaques</h1>
@stop

@section('breadcrumb')
    <li class="breadcrumb-item active"> Destaques</li>

@endsection

@section('acoes')
    <a href="{{ route('home') }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Voltar">
        <i class="fa fa-reply"></i> Voltar
    </a>
@endsection

@section('content')
@section('plugins.Summernote', true)
@section('plugins.Croppie', true)


@include('mensagem')

<form action="{{ route('destaques.store') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fa fa-star"></i>
                Destaque 1
            </h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body table-responsive">
            <div class="row">
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Título 1 *</label>
                    <input type="text" name="titulo_1"
                        class="form-control {{ $errors->has('titulo_1') ? 'is-invalid' : '' }}"
                        value="{{ old('titulo_1') ?? ($registro->titulo_1 ?? null) }}" placeholder="Título" autofocus>
                    @if ($errors->has('titulo_1'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('titulo_1') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Subtítulo 1</label>
                    <input type="text" name="subtitulo_1"
                        class="form-control {{ $errors->has('subtitulo_1') ? 'is-invalid' : '' }}"
                        value="{{ old('subtitulo_1') ?? ($registro->subtitulo_1 ?? null) }}" placeholder="Subtítulo">
                    @if ($errors->has('subtitulo_1'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('subtitulo_1') }}</strong>
                        </div>
                    @endif
                </div>

                <div class="col-md-5 col-lg-5">
                    <div class="row">
                        <div class="img-content">
                            <div class="col-md-12">
                                <label for="exampleInputFile">Imagem 1</label>
                                (350 x 360)

                                <div class="alert alert-info" id="aviso-remover" @if (!isset($registro) || (isset($registro) && $registro->imagemn1 == null)) style="display: none;" @endif>
                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <small>
                                        Caso deseje escolher outra imagem clique no botão "Remover Imagem" e em
                                        seguida clique em "Selecionar Imagem".
                                    </small>
                                </div>

                                <div class="alert alert-info" id="aviso-mover" @if (isset($registro) && $registro->imagemn1 != null) style="display: none;" @endif>

                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>

                                    <small>
                                        Se desejar, clique e arraste a foto para cima ou para baixo para
                                        reposicioná-la
                                    </small>
                                </div>

                                <div class="img-foto-1-wrap">

                                    @if (isset($registro) && $registro->imagemn1 != null)
                                        <img class=" foto img-responsive" src="{{ $registro->getImagemn1Attribute() }}"
                                            alt="Imagem Principal">
                                    @endif
                                    <div id="img-foto-1"
                                        data-img1-default-foto="{{ asset('imagem/site/default-image.jpg') }}"
                                        @if (isset($registro) && $registro->imagemn1 != null) style="display: none;" @endif></div>
                                    <input type="hidden" id="image1base64" name="image1base64" />
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <a href="#" id="btn-remove-img-foto-1"
                                    class="jFiler-item-trash-action btn btn-sm btn-flat btn-danger btn-file"
                                    @if (isset($registro) && $registro->imagemn1 == null) style="display: none;" @endif>
                                    <i class="fa fa-trash" aria-hidden="true"></i> Remover Imagem
                                </a>
                                <div id="btn-upload-img-foto-1" class="btn btn-sm btn-flat btn-primary btn-file "
                                    @if (isset($registro) && $registro->imagemn1 != null) style="display: none;" @endif>


                                    <i class="fa fa-camera" aria-hidden="true"></i> Selecionar Imagem
                                    <input type="file" name="arquivo_1" id="upload-img-foto-1" accept="image/*">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-7 mb-3">
                    <label>Texto 1*</label>
                    <textarea class="textarea form-control {{ $errors->has('texto_1') ? 'is-invalid' : '' }}"
                        type="textarea" name="texto_1">
                    {!! isset($registro->texto_1) ? $registro->texto_1 : old('texto_1') !!}
                </textarea>
                    @if ($errors->has('texto_1'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('texto_1') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fa fa-star"></i>
                Destaque 2
            </h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body table-responsive">
            <div class="row">
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Título 2 *</label>
                    <input type="text" name="titulo_2"
                        class="form-control {{ $errors->has('titulo_2') ? 'is-invalid' : '' }}"
                        value="{{ old('titulo_2') ?? ($registro->titulo_2 ?? null) }}" placeholder="Título"
                        autofocus>
                    @if ($errors->has('titulo_2'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('titulo_2') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Subtítulo 2</label>
                    <input type="text" name="subtitulo_2"
                        class="form-control {{ $errors->has('subtitulo_2') ? 'is-invalid' : '' }}"
                        value="{{ old('subtitulo_2') ?? ($registro->subtitulo_2 ?? null) }}" placeholder="Subtítulo">
                    @if ($errors->has('subtitulo_2'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('subtitulo_2') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Texto 2*</label>
                    <textarea type="textarea" name="texto_2"
                        class="textarea2 form-control {{ $errors->has('texto_2') ? 'is-invalid' : '' }}">
                        {!! isset($registro->texto_2) ? $registro->texto_2 : old('texto_2') !!}
                    </textarea>
                    @if ($errors->has('texto_2'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('texto_2') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="img-content">
                            <div class="col-md-12">
                                <label for="exampleInputFile">Imagem 2</label>
                                (730 x 360)
                                <div class="alert alert-info" id="aviso-remover" @if (!isset($registro) || (isset($registro) && $registro->imagemn2 == null)) style="display: none;" @endif>
                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <small>
                                        Caso deseje escolher outra imagem clique no botão "Remover Imagem" e em
                                        seguida clique em "Selecionar Imagem".
                                    </small>
                                </div>

                                <div class="alert alert-info" id="aviso-mover" @if (isset($registro) && $registro->imagemn2 != null) style="display: none;" @endif>

                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>

                                    <small>
                                        Se desejar, clique e arraste a foto para cima ou para baixo para
                                        reposicioná-la
                                    </small>
                                </div>

                                <div class="img-foto-2-wrap">

                                    @if (isset($registro) && $registro->imagemn2 != null)
                                        <img class=" foto img-responsive" src="{{ $registro->getImagemn2Attribute() }}"
                                            alt="Imagem Principal">
                                    @endif
                                    <div id="img-foto-2"
                                        data-img2-default-foto="{{ asset('imagem/site/default-image.jpg') }}"
                                        @if (isset($registro) && $registro->imagemn2 != null) style="display: none;" @endif></div>
                                    <input type="hidden" id="image2base64" name="image2base64" />

                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <a href="#" id="btn-remove-img-foto-2"
                                    class="jFiler-item-trash-action btn btn-sm btn-flat btn-danger btn-file"
                                    @if (isset($registro) && $registro->imagemn2 == null) style="display: none;" @endif>
                                    <i class="fa fa-trash" aria-hidden="true"></i> Remover Imagem
                                </a>
                                <div id="btn-upload-img-foto-2" class="btn btn-sm btn-flat btn-primary btn-file "
                                    @if (isset($registro) && $registro->imagemn2 != null) style="display: none;" @endif>
                                    <i class="fa fa-camera" aria-hidden="true"></i> Selecionar Imagem
                                    <input type="file" name="arquivo_2" id="upload-img-foto-2" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fa fa-star"></i>
                Destaque 3
            </h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body table-responsive">
            <div class="row">
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Título 3 *</label>
                    <input type="text" name="titulo_3"
                        class="form-control {{ $errors->has('titulo_3') ? 'is-invalid' : '' }}"
                        value="{{ old('titulo_3') ?? ($registro->titulo_3 ?? null) }}" placeholder="Título"
                        autofocus>
                    @if ($errors->has('titulo_3'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('titulo_3') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Subtítulo 3</label>
                    <input type="text" name="subtitulo_3"
                        class="form-control {{ $errors->has('subtitulo_3') ? 'is-invalid' : '' }}"
                        value="{{ old('subtitulo_3') ?? ($registro->subtitulo_3 ?? null) }}" placeholder="Subtítulo">
                    @if ($errors->has('subtitulo_3'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('subtitulo_3') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-3">
                    <label>Texto 3*</label>
                    <textarea type="textarea" name="texto_3"
                        class="textarea3 form-control {{ $errors->has('texto_3') ? 'is-invalid' : '' }}">
                        {!! isset($registro->texto_3) ? $registro->texto_3 : old('texto_3') !!}
                    </textarea>
                    @if ($errors->has('texto_3'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('texto_3') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="img-content">
                            <div class="col-md-12">
                                <label for="exampleInputFile">Imagem 3</label>
                                (635 x 410)
                                <div class="alert alert-info" id="aviso-remover" @if (!isset($registro) || (isset($registro) && $registro->imagemn3 == null)) style="display: none;" @endif>
                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <small>
                                        Caso deseje escolher outra imagem clique no botão "Remover Imagem" e em
                                        seguida clique em "Selecionar Imagem".
                                    </small>
                                </div>

                                <div class="alert alert-info" id="aviso-mover" @if (isset($registro) && $registro->imagemn3 != null) style="display: none;" @endif>

                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>

                                    <small>
                                        Se desejar, clique e arraste a foto para cima ou para baixo para
                                        reposicioná-la
                                    </small>
                                </div>

                                <div class="img-foto-3-wrap">

                                    @if (isset($registro) && $registro->imagemn3 != null)
                                        <img class=" foto img-responsive" src="{{ $registro->getImagemn3Attribute() }}"
                                            alt="Imagem Principal">
                                    @endif
                                    <div id="img-foto-3"
                                        data-img3-default-foto="{{ asset('imagem/site/default-image.jpg') }}"
                                        @if (isset($registro) && $registro->imagemn3 != null) style="display: none;" @endif></div>
                                    <input type="hidden" id="image3base64" name="image3base64" />

                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <a href="#" id="btn-remove-img-foto-3"
                                    class="jFiler-item-trash-action btn btn-sm btn-flat btn-danger btn-file"
                                    @if (isset($registro) && $registro->imagemn3 == null) style="display: none;" @endif>
                                    <i class="fa fa-trash" aria-hidden="true"></i> Remover Imagem
                                </a>
                                <div id="btn-upload-img-foto-3" class="btn btn-sm btn-flat btn-primary btn-file "
                                    @if (isset($registro) && $registro->imagemn3 != null) style="display: none;" @endif>
                                    <i class="fa fa-camera" aria-hidden="true"></i> Selecionar Imagem
                                    <input type="file" name="arquivo_3" id="upload-img-foto-3" accept="image/*">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h4 class="card-title">
                <i class="fa fa-star"></i>
                Destaque 4
            </h4>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body table-responsive">
            <div class="row">
                <div class="col-xs-12 col-md-12 mb-4">
                    <label>Título 4 *</label>
                    <input type="text" name="titulo_4"
                        class="form-control {{ $errors->has('titulo_4') ? 'is-invalid' : '' }}"
                        value="{{ old('titulo_4') ?? ($registro->titulo_4 ?? null) }}" placeholder="Título" autofocus>
                    @if ($errors->has('titulo_4'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('titulo_4') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="col-xs-12 col-md-12 mb-4">
                    <label>Subtítulo 4</label>
                    <input type="text" name="subtitulo_4"
                        class="form-control {{ $errors->has('subtitulo_4') ? 'is-invalid' : '' }}"
                        value="{{ old('subtitulo_4') ?? ($registro->subtitulo_4 ?? null) }}" placeholder="Subtítulo">
                    @if ($errors->has('subtitulo_4'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('subtitulo_4') }}</strong>
                        </div>
                    @endif
                </div>

                <div class="col-md-6 col-lg-6">
                    <div class="row">
                        <div class="img-content">
                            <div class="col-md-12">
                                <label for="exampleInputFile">Imagem 4</label>
                                (445 x 386)

                                <div class="alert alert-info" id="aviso-remover" @if (!isset($registro) || (isset($registro) && $registro->imagemn4 == null)) style="display: none;" @endif>
                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>
                                    <small>
                                        Caso deseje escolher outra imagem clique no botão "Remover Imagem" e em
                                        seguida clique em "Selecionar Imagem".
                                    </small>
                                </div>

                                <div class="alert alert-info" id="aviso-mover" @if (isset($registro) && $registro->imagemn4 != null) style="display: none;" @endif>

                                    <i class="fa fa-info-circle"></i>
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">×</button>

                                    <small>
                                        Se desejar, clique e arraste a foto para cima ou para baixo para
                                        reposicioná-la
                                    </small>
                                </div>

                                <div class="img-foto-4-wrap">

                                    @if (isset($registro) && $registro->imagemn4 != null)
                                        <img class=" foto img-responsive" src="{{ $registro->getImagemn4Attribute() }}"
                                            alt="Imagem Principal">
                                    @endif
                                    <div id="img-foto-4"
                                        data-img4-default-foto="{{ asset('imagem/site/default-image.jpg') }}"
                                        @if (isset($registro) && $registro->imagemn4 != null) style="display: none;" @endif></div>
                                    <input type="hidden" id="image4base64" name="image4base64" />
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <a href="#" id="btn-remove-img-foto-4"
                                    class="jFiler-item-trash-action btn btn-sm btn-flat btn-danger btn-file"
                                    @if (isset($registro) && $registro->imagemn4 == null) style="display: none;" @endif>
                                    <i class="fa fa-trash" aria-hidden="true"></i> Remover Imagem
                                </a>
                                <div id="btn-upload-img-foto-4" class="btn btn-sm btn-flat btn-primary btn-file "
                                    @if (isset($registro) && $registro->imagemn4 != null) style="display: none;" @endif>


                                    <i class="fa fa-camera" aria-hidden="true"></i> Selecionar Imagem
                                    <input type="file" name="arquivo_4" id="upload-img-foto-4" accept="image/*">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 mb-4">
                    <label>Texto 4*</label>
                    <textarea class="textarea4 form-control {{ $errors->has('texto_4') ? 'is-invalid' : '' }}"
                        type="textarea" name="texto_4">
                    {!! isset($registro->texto_4) ? $registro->texto_4 : old('texto_4') !!}
                </textarea>
                    @if ($errors->has('texto_4'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('texto_4') }}</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="text-right">
            <a href="{{ route('home') }}" class="btn btn-default">
                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
            </a>
            <button class="btn btn-success btn-submit" type="submit">
                <i class="fa fa-save"></i> Salvar
            </button>
        </div>
    </div>
</form>
@stop
@section('js')
<script src="{{ asset('vendor/summernote/lang/summernote-pt-BR.js') }}"></script>
<script>
    $(document).ready(function() {
        imgFoto1();
        imgFoto2();
        imgFoto3();
        imgFoto4();
        $(".textarea").summernote({
            lang: "pt-BR",
            height: 300
        });

        $(".textarea2").summernote({
            lang: "pt-BR",
            height: 200
        });

        $(".textarea3").summernote({
            lang: "pt-BR",
            height: 200
        });

        $(".textarea4").summernote({
            lang: "pt-BR",
            height: 400
        });
    });

    //croopie
    function imgFoto1() {
        let $uploadCrop;
        let config = {
            viewport: {
                width: 350,
                height: 360,
                type: 'square'
            },
            boundary: {
                width: 350,
                height: 360
            },
            enableExif: true
        };

        function readFile(input) {

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#img-foto-1').croppie('destroy');
                    $('#img-foto-1').croppie(config);
                    $uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function(res) {
                        $('#btn-upload-img-foto-1').hide();
                        $('#aviso-remover').hide();

                        $('#btn-remove-img-foto-1').show();
                        $('#aviso-mover').show();
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal(
                    "Desculpe - o navegador não suporta a API FileReader! Use um navegador mais moderno como Chrome ou Firefox!"
                );
            }
        }

        $uploadCrop = $('#img-foto-1').croppie(config);
        if ($uploadCrop.length > 0) {
            setImgDefaultFoto1($uploadCrop);
        }

        $('#upload-img-foto-1').on('change', function() {

            //console.log(this);
            // Verifica se #img-foto-1 é visivel
            if ($('.img-foto-1-wrap').children('img').length > 0 && $('.img-foto-1-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-1-wrap').children('img').hide();
                $('#img-foto-1').show();
            }
            readFile(this);
        });

        $('#btn-remove-img-foto-1').on('click', function(e) {
            e.preventDefault();
            if ($('.img-foto-1-wrap').children('img').length > 0 && $('.img-foto-1-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-1-wrap').children('img').hide();
                $('#img-foto-1').show();
                $('#aviso-remover').hide();
            }
            setImgDefaultFoto1($uploadCrop);
        });

        // Seta imagem quando clicado em btn-submit
        $('.btn-submit').on('click', function(ev) {

            // Verifica se o o btn-remove é visivel, se for é feito o result do croppie
            if ($('#btn-remove-img-foto-1:visible').length > 0 && $('.img-foto-1-wrap').children('img').is(
                    ':visible') == false) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                }).then(function(resp) {
                    $('#image1base64').val(resp);
                });
            }
        });
    }

    function setImgDefaultFoto1($uploadCrop) {
        let imgDefaultFoto = ($('#img-foto-1:visible').data('img1-default-foto')) ? $('#img-foto-1').data(
            'img1-default-foto') : null;
        if (imgDefaultFoto != null) {
            $('#image1base64').val('default.png');
            $('#upload-img-foto-1').val('');


            $('#btn-upload-img-foto-1').show();
            //$('#aviso-remover').show();

            $('#btn-remove-img-foto-1').hide();
            $('#aviso-mover').hide();

            $uploadCrop.croppie('bind', {
                url: imgDefaultFoto,
                //points: [0, 50,0,50],
            });
        }
    }

    function imgFoto2() {
        let $uploadCrop;
        let config = {
            viewport: {
                width: 730,
                height: 360,
                type: 'square'
            },
            boundary: {
                width: 730,
                height: 360
            },
            enableExif: true
        };

        function readFile(input) {

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#img-foto-2').croppie('destroy');
                    $('#img-foto-2').croppie(config);
                    $uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function(res) {
                        $('#btn-upload-img-foto-2').hide();
                        $('#aviso-remover').hide();

                        $('#btn-remove-img-foto-2').show();
                        $('#aviso-mover').show();
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal(
                    "Desculpe - o navegador não suporta a API FileReader! Use um navegador mais moderno como Chrome ou Firefox!"
                );
            }
        }

        $uploadCrop = $('#img-foto-2').croppie(config);
        if ($uploadCrop.length > 0) {
            setImgDefaultFoto2($uploadCrop);
        }

        $('#upload-img-foto-2').on('change', function() {

            //console.log(this);
            // Verifica se #img-foto-2 é visivel
            if ($('.img-foto-2-wrap').children('img').length > 0 && $('.img-foto-2-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-2-wrap').children('img').hide();
                $('#img-foto-2').show();
            }
            readFile(this);
        });

        $('#btn-remove-img-foto-2').on('click', function(e) {
            e.preventDefault();
            if ($('.img-foto-2-wrap').children('img').length > 0 && $('.img-foto-2-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-2-wrap').children('img').hide();
                $('#img-foto-2').show();
                $('#aviso-remover').hide();
            }
            setImgDefaultFoto2($uploadCrop);
        });

        // Seta imagem quando clicado em btn-submit
        $('.btn-submit').on('click', function(ev) {

            // Verifica se o o btn-remove é visivel, se for é feito o result do croppie
            if ($('#btn-remove-img-foto-2:visible').length > 0 && $('.img-foto-2-wrap').children('img').is(
                    ':visible') == false) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                }).then(function(resp) {
                    $('#image2base64').val(resp);
                });
            }
        });
    }

    function setImgDefaultFoto2($uploadCrop) {
        let imgDefaultFoto = ($('#img-foto-2:visible').data('img2-default-foto')) ? $('#img-foto-2').data(
            'img2-default-foto') : null;
        if (imgDefaultFoto != null) {
            $('#image2base64').val('default.png');
            $('#upload-img-foto-2').val('');


            $('#btn-upload-img-foto-2').show();
            //$('#aviso-remover').show();

            $('#btn-remove-img-foto-2').hide();
            $('#aviso-mover').hide();

            $uploadCrop.croppie('bind', {
                url: imgDefaultFoto,
                //points: [0, 50,0,50],
            });
        }
    }

    function imgFoto3() {
        let $uploadCrop;
        let config = {
            viewport: {
                width: 635,
                height: 410,
                type: 'square'
            },
            boundary: {
                width: 635,
                height: 410
            },
            enableExif: true
        };

        function readFile(input) {

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#img-foto-3').croppie('destroy');
                    $('#img-foto-3').croppie(config);
                    $uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function(res) {
                        $('#btn-upload-img-foto-3').hide();
                        $('#aviso-remover').hide();

                        $('#btn-remove-img-foto-3').show();
                        $('#aviso-mover').show();
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal(
                    "Desculpe - o navegador não suporta a API FileReader! Use um navegador mais moderno como Chrome ou Firefox!"
                );
            }
        }

        $uploadCrop = $('#img-foto-3').croppie(config);
        if ($uploadCrop.length > 0) {
            setImgDefaultFoto3($uploadCrop);
        }

        $('#upload-img-foto-3').on('change', function() {

            //console.log(this);
            // Verifica se #img-foto-3 é visivel
            if ($('.img-foto-3-wrap').children('img').length > 0 && $('.img-foto-3-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-3-wrap').children('img').hide();
                $('#img-foto-3').show();
            }
            readFile(this);
        });

        $('#btn-remove-img-foto-3').on('click', function(e) {
            e.preventDefault();
            if ($('.img-foto-3-wrap').children('img').length > 0 && $('.img-foto-3-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-3-wrap').children('img').hide();
                $('#img-foto-3').show();
                $('#aviso-remover').hide();
            }
            setImgDefaultFoto3($uploadCrop);
        });

        // Seta imagem quando clicado em btn-submit
        $('.btn-submit').on('click', function(ev) {

            // Verifica se o o btn-remove é visivel, se for é feito o result do croppie
            if ($('#btn-remove-img-foto-3:visible').length > 0 && $('.img-foto-3-wrap').children('img').is(
                    ':visible') == false) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                }).then(function(resp) {
                    $('#image3base64').val(resp);
                });
            }
        });
    }

    function setImgDefaultFoto3($uploadCrop) {
        let imgDefaultFoto = ($('#img-foto-3:visible').data('img3-default-foto')) ? $('#img-foto-3').data(
            'img3-default-foto') : null;
        if (imgDefaultFoto != null) {
            $('#image3base64').val('default.png');
            $('#upload-img-foto-3').val('');


            $('#btn-upload-img-foto-3').show();
            //$('#aviso-remover').show();

            $('#btn-remove-img-foto-3').hide();
            $('#aviso-mover').hide();

            $uploadCrop.croppie('bind', {
                url: imgDefaultFoto,
                //points: [0, 50,0,50],
            });
        }
    }

    function imgFoto4() {
        let $uploadCrop;
        let config = {
            viewport: {
                width: 445,
                height: 386,
                type: 'square'
            },
            boundary: {
                width: 445,
                height: 386
            },
            enableExif: true
        };

        function readFile(input) {

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#img-foto-4').croppie('destroy');
                    $('#img-foto-4').croppie(config);
                    $uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function(res) {
                        $('#btn-upload-img-foto-4').hide();
                        $('#aviso-remover').hide();

                        $('#btn-remove-img-foto-4').show();
                        $('#aviso-mover').show();
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal(
                    "Desculpe - o navegador não suporta a API FileReader! Use um navegador mais moderno como Chrome ou Firefox!"
                );
            }
        }

        $uploadCrop = $('#img-foto-4').croppie(config);
        if ($uploadCrop.length > 0) {
            setImgDefaultFoto4($uploadCrop);
        }

        $('#upload-img-foto-4').on('change', function() {

            //console.log(this);
            // Verifica se #img-foto-4 é visivel
            if ($('.img-foto-4-wrap').children('img').length > 0 && $('.img-foto-4-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-4-wrap').children('img').hide();
                $('#img-foto-4').show();
            }
            readFile(this);
        });

        $('#btn-remove-img-foto-4').on('click', function(e) {
            e.preventDefault();
            if ($('.img-foto-4-wrap').children('img').length > 0 && $('.img-foto-4-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-4-wrap').children('img').hide();
                $('#img-foto-4').show();
                $('#aviso-remover').hide();
            }
            setImgDefaultFoto4($uploadCrop);
        });

        // Seta imagem quando clicado em btn-submit
        $('.btn-submit').on('click', function(ev) {

            // Verifica se o o btn-remove é visivel, se for é feito o result do croppie
            if ($('#btn-remove-img-foto-4:visible').length > 0 && $('.img-foto-4-wrap').children('img').is(
                    ':visible') == false) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                }).then(function(resp) {
                    $('#image4base64').val(resp);
                });
            }
        });
    }

    function setImgDefaultFoto4($uploadCrop) {
        let imgDefaultFoto = ($('#img-foto-4:visible').data('img4-default-foto')) ? $('#img-foto-4').data(
            'img4-default-foto') : null;
        if (imgDefaultFoto != null) {
            $('#image4base64').val('default.png');
            $('#upload-img-foto-4').val('');


            $('#btn-upload-img-foto-4').show();
            //$('#aviso-remover').show();

            $('#btn-remove-img-foto-4').hide();
            $('#aviso-mover').hide();

            $uploadCrop.croppie('bind', {
                url: imgDefaultFoto,
                //points: [0, 50,0,50],
            });
        }
    }
</script>
@endsection
