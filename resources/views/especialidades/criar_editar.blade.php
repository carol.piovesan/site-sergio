@extends('layout.base')

@section('title', 'Especialidades')

@section('content_header')
    <h1>Especialidades</h1>
@stop

@section('breadcrumb')
    <li class="breadcrumb-item active"> Especialidades</li>

@endsection

@section('acoes')
    <a href="{{ route('home') }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Voltar">
        <i class="fa fa-reply"></i> Voltar
    </a>
@endsection

@section('content')
@section('plugins.Summernote', true)
@section('plugins.Croppie', true)

<div class="row">
    <div class="col-md-12">
         @include('mensagem') 
        <div class="card">
            <div class="card-body table-responsive">
                <form action="{{ route('especialidades.store') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $registro->id ?? 0 }}">

                    <div class="row">
                        <div class="col-xs-12 col-md-12 mb-3">

                            <label>Texto</label>
                            <textarea type="textarea" name="texto"
                                class="textarea form-control {{ $errors->has('texto') ? 'is-invalid' : '' }}">

                            @if (isset($registro->texto))
                            {!! $registro->texto !!}
                @else
                            {!! old('texto') !!}
                            @endif
                        </textarea>
                            @if ($errors->has('texto'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('texto') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="text-right">
                            <a href="{{ route('home') }}" class="btn btn-default">
                                <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
                            </a>
                            <button class="btn btn-success btn-submit" type="submit">
                                <i class="fa fa-save"></i> Salvar
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{ asset('vendor/summernote/lang/summernote-pt-BR.js') }}"></script>
<script>
    $(document).ready(function() {
       
        $(".textarea").summernote({
            lang: "pt-BR",
            height: 300
        });
    });
    </script>
@endsection