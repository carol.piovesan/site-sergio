@extends('layout.base')

@section('title', 'Painel Administrativo')

@section('content_header')
<h1>Painel Administrativo</h1>
@stop

@section('content')
<style type="text/css">
	.info-box-text, .info-box-number {
    color: #212529;
}
</style>
<div class="row">
	<div class="col-lg-4 col-md-6 col-sm-12">
		<a href="{{ route('posts.index') }}">
			<div class="info-box ">
				<span class="info-box-icon bg-green"><i class="far fa-fw fa-file "></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Posts</span>
					<span class="info-box-number">{{$posts}}</span>
				</div>
			</div>
		</a>
	</div>
</div>

@stop

