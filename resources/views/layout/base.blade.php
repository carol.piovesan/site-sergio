<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    {{-- Base Meta Tags --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') . Sergio Duarte Junior</title>

    <meta name="description" content="Sergio Duarte Junior" />
    <meta name="keywords" content="painel administrativo" />
    <meta name="robots" content="noindex">
    <meta name="author" content="Sergio Duarte Junior">
    <link rel="shortcut icon" href="{{ asset('imagem/site/fav.png') }}" type="image/x-icon" />

    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/overlayScrollbars/css/OverlayScrollbars.min.css') }}">


    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @include('adminlte::plugins', ['type' => 'css'])
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">
</head>


<body class="sidebar-mini layout-fixed">
    @yield('css')
    <div class="wrapper">
        @include('layout.navbar')
        @include('layout.sidebar')

        <div class="content-wrapper">

            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-7">
                            <h1 class="m-0 text-dark"> @yield('content_header')</h1>
                        </div>
                        <div class="col-sm-5">
                            <div class=" float-sm-right">
                                @yield('acoes')
                            </div>
                        </div>
                        <div class="col-sm-12 mt-2">
                            <ol class="breadcrumb  float-sm-left">
                                <li class="breadcrumb-item"><a href="#">Início</a></li>
                                @yield('breadcrumb')
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <footer class="main-footer">
        <strong>Copyright © {{ date('Y') }} <a href="#">{{ config('app.name') }} </a>.</strong>Todos os direitos
        reservados. - <b>Versão</b> {{ config('ao.versao') }}
    </footer>
    <a id="topo" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Voltar ao topo">
        <i class="fas fa-chevron-up"></i>
    </a>
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>


    @include('adminlte::plugins', ['type' => 'js'])

    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @yield('js')

    @yield('js_extra')

    @yield('js_extra_2')

</body>

</html>
