<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ (Route::currentRouteName() == 'painel.inicio' ) ? 'active' : null  }}">
        <i class="nav-icon fa fa-home"></i>
        <p>
            Início
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('destaques.create') }}" class="nav-link {{ strpos(Route::currentRouteName(), 'destaques.') !== false  ? 'active' : null  }}">
        <i class="nav-icon fas fa-star"></i>
        <p>
            Destaques
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('especialidades.create') }}" class="nav-link {{ strpos(Route::currentRouteName(), 'especialidades.') !== false  ? 'active' : null  }}">
        <i class="nav-icon fas fa-list"></i>
        <p>
            Especialidades
        </p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('posts.index') }}" class="nav-link {{ strpos(Route::currentRouteName(), 'posts.') !== false  ? 'active' : null  }}">
        <i class="nav-icon fas fa-file"></i>
        <p>
            Posts
        </p>
    </a>
</li>

