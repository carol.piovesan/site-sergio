<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    <a href="{{ route('home') }}" class="brand-link" />


    {{-- Small brand logo --}}
    <img src="{{ asset('imagem/site/psicologia-curso.png') }}"
    alt="{{ config('app.name') }}"
    class="{{ config('adminlte.logo_img_class', 'brand-image img-circle elevation-3') }}">

    {{-- Brand text --}}
    <span class="brand-text font-weight-light {{ config('adminlte.classes_brand_text') }}">
       <b>Sérgio</b> Duarte Junior
   </span>
</a>

{{-- Sidebar menu --}}
<div class="sidebar">
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

      @include('layout.menu')

    {{-- Configured sidebar links
        @each('adminlte::partials.sidebar.menu-item', $adminlte->menu('sidebar'), 'item')
        --}}
    </ul>
</nav>
</div>

</aside>
