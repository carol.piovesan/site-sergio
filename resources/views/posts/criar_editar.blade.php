@extends('layout.base')

@section('title', 'Posts')

@section('content_header')
    <h1>Posts</h1>
@stop

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">Posts</a></li>
    <li class="breadcrumb-item active"> {{ isset($registro) ? 'Editar' : 'Novo' }} </li>

@endsection

@section('acoes')
    <a href="{{ route('home') }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Voltar">
        <i class="fa fa-reply"></i> Voltar
    </a>
@endsection

@section('content')
@section('plugins.Summernote', true)
@section('plugins.Croppie', true)

<div class="row">
    <div class="col-md-12">
        {{-- @include('tema::mensagem') --}}
        <div class="card">
            <div class="card-body table-responsive">
                @if (isset($registro))
                    <form action="{{ route('posts.update', $registro->id) }}" method="post"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">

                @endif

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="col-xs-12 col-md-12 mb-3">
                        <label>Título</label>
                        <input type="text" name="titulo"
                            class="form-control {{ $errors->has('titulo') ? 'is-invalid' : '' }}"
                            value="{{ old('titulo') ?? ($registro->titulo ?? null) }}" placeholder="Título" autofocus>
                        @if ($errors->has('titulo'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('titulo') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-12 col-md-9 mb-3">
                        <label>Subtítulo</label>
                        <input type="text" name="subtitulo"
                            class="form-control {{ $errors->has('subtitulo') ? 'is-invalid' : '' }}"
                            value="{{ old('subtitulo') ?? ($registro->subtitulo ?? null) }}" placeholder="Subtítulo">
                        @if ($errors->has('subtitulo'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('subtitulo') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group {{ $errors->has('data_hora') ? 'has-error' : null }}">
                            <label>Data do Post </label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input name="data_hora" placeholder="00/00/0000" type="text"
                                    class="form-control {{ $errors->has('data_hora') ? 'is-invalid' : '' }} mask"
                                    data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask
                                    value="{{ old('data_hora') ?? ($registro->data_hora ?? null) }}">
                                @if ($errors->has('data_hora'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('data_hora') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-5">
                        <div class="row">
                            <div class="img-content">
                                <div class="col-md-12">
                                    <label for="exampleInputFile">Imagem</label>
                                    (350 x 255)

                                    <div class="alert alert-info" id="aviso-remover" @if (!isset($registro) || (isset($registro) && $registro->capa == null)) style="display: none;" @endif>
                                        <i class="fa fa-info-circle"></i>
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">×</button>
                                        <small>
                                            Caso deseje escolher outra imagem clique no botão "Remover Imagem" e em
                                            seguida clique em "Selecionar Imagem".
                                        </small>
                                    </div>

                                    <div class="alert alert-info" id="aviso-mover" @if (isset($registro) && $registro->capa != null) style="display: none;" @endif>

                                        <i class="fa fa-info-circle"></i>
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">×</button>

                                        <small>
                                            Se desejar, clique e arraste a foto para cima ou para baixo para
                                            reposicioná-la
                                        </small>
                                    </div>


                                    <div class="img-foto-wrap">

                                        @if (isset($registro) && $registro->capa != null)
                                            <img class=" foto img-responsive"
                                                src="{{ $registro->getImagemAttribute() }}" alt="Imagem Principal">
                                        @endif
                                        <div id="img-foto"
                                            data-img-default-foto="{{ asset('imagem/site/default-image.jpg') }}"
                                            @if (isset($registro) && $registro->capa != null) style="display: none;" @endif></div>
                                        <input type="hidden" id="imagebase64" name="imagebase64" />

                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px">
                                    <a href="#" id="btn-remove-img-foto"
                                        class="jFiler-item-trash-action btn btn-sm btn-flat btn-danger btn-file"
                                        @if (isset($registro) && $registro->capa == null) style="display: none;" @endif>
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Remover Imagem
                                    </a>
                                    <div id="btn-upload-img-foto" class="btn btn-sm btn-flat btn-primary btn-file "
                                        @if (isset($registro) && $registro->capa != null) style="display: none;" @endif>


                                        <i class="fa fa-camera" aria-hidden="true"></i> Selecionar Imagem
                                        <input type="file" name="arquivo_principal" id="upload-img-foto"
                                            accept="image/*">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-7 mb-3">

                        <label>Texto</label>
                        <textarea type="textarea" name="texto"
                            class="textarea form-control {{ $errors->has('texto') ? 'is-invalid' : '' }}">

                            @if (isset($registro->texto))
                            {!! $registro->texto !!}
                        @else
                            {!! old('texto') !!}
                            @endif
                        </textarea>
                        @if ($errors->has('texto'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('texto') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="box-footer">
                    <div class="text-right">
                        <a href="{{ route('home') }}" class="btn btn-default">
                            <i class="fa fa-angle-double-left" aria-hidden="true"></i> Voltar
                        </a>
                        <button class="btn btn-success btn-submit" type="submit">
                            <i class="fa fa-save"></i> Salvar
                        </button>

                    </div>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>
@stop

@section('css')

<style type="text/css">
    .custom-file-label::after {
        content: "Selecionar";
    }

</style>
@stop

@section('js')
<script src="{{ asset('vendor/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('vendor/summernote/lang/summernote-pt-BR.js') }}"></script>
<script src="{{ asset('vendor/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.mask').inputmask('##/##/###', {
            'placeholder': '__/__/_____'
        });

        bsCustomFileInput.init();


        $(".textarea").summernote({
            lang: "pt-BR",
            height: 300
        });

        imgFoto();

    });

    //croopie
    function imgFoto() {
        let $uploadCrop;
        let config = {
            viewport: {
                width: 350,
                height: 255,
                type: 'square'
            },
            boundary: {
                width: 350,
                height: 255
            },
            enableExif: true
        };

        function readFile(input) {

            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#img-foto').croppie('destroy');
                    $('#img-foto').croppie(config);
                    $uploadCrop.croppie('bind', {
                        url: e.target.result,
                    }).then(function(res) {
                        $('#btn-upload-img-foto').hide();
                        $('#aviso-remover').hide();

                        $('#btn-remove-img-foto').show();
                        $('#aviso-mover').show();
                    });
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                swal(
                    "Desculpe - o navegador não suporta a API FileReader! Use um navegador mais moderno como Chrome ou Firefox!");
            }
        }

        $uploadCrop = $('#img-foto').croppie(config);
        if ($uploadCrop.length > 0) {
            setImgDefaultFoto($uploadCrop);
        }

        $('#upload-img-foto').on('change', function() {

            //console.log(this);
            // Verifica se #img-foto é visivel
            if ($('.img-foto-wrap').children('img').length > 0 && $('.img-foto-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-wrap').children('img').hide();
                $('#img-foto').show();
            }
            readFile(this);
        });

        $('#btn-remove-img-foto').on('click', function(e) {
            e.preventDefault();
            if ($('.img-foto-wrap').children('img').length > 0 && $('.img-foto-wrap').children('img').is(
                    ':visible') == true) {
                $('.img-foto-wrap').children('img').hide();
                $('#img-foto').show();
                $('#aviso-remover').hide();
            }
            setImgDefaultFoto($uploadCrop);
        });

        // Seta imagem quando clicado em btn-submit
        $('.btn-submit').on('click', function(ev) {

            // Verifica se o o btn-remove é visivel, se for é feito o result do croppie
            if ($('#btn-remove-img-foto:visible').length > 0 && $('.img-foto-wrap').children('img').is(
                    ':visible') == false) {
                $uploadCrop.croppie('result', {
                    type: 'base64',
                    size: 'viewport',
                }).then(function(resp) {
                    $('#imagebase64').val(resp);
                });
            }
        });
    }
    

    function setImgDefaultFoto($uploadCrop) {
        let imgDefaultFoto = ($('#img-foto:visible').data('img-default-foto')) ? $('#img-foto').data(
            'img-default-foto') : null;
        if (imgDefaultFoto != null) {
            $('#imagebase64').val('default.png');
            $('#upload-img-foto').val('');


            $('#btn-upload-img-foto').show();
            //$('#aviso-remover').show();

            $('#btn-remove-img-foto').hide();
            $('#aviso-mover').hide();

            $uploadCrop.croppie('bind', {
                url: imgDefaultFoto,
                //points: [0, 50,0,50],
            });
        }
    }
</script>
@stop
