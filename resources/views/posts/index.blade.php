@extends('layout.base')

@section('title', 'Posts')

@section('content_header')
<h1>Posts</h1>
@stop

@section('breadcrumb')
<li class="breadcrumb-item active">Posts</li>
@endsection

@section('acoes')
<a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Novo">
  <i class="fa fa-plus"></i>
  Novo
</a>
<a href="{{ route('home') }}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Voltar">
  <i class="fa fa-reply"></i> Voltar
</a>
@endsection

@section('content')
@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
<div class="row">
    <div class="col-md-12">
        @include('mensagem') 
        <div class="card">     
            <div class="card-body table-responsive">                
                <table class="table table-bordered table-hover dataTable dtr-inline">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Data do Post</th>
                            <th>Criado em</th>
                            <th class="text-right no-print">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($registros as $registro)
                        <tr>
                            <td>{{ $registro->id }}</td>
                            <td> <a href="#" target="_blank">{{ $registro->titulo }}</a></td>

                            <td> {{ $registro->data_hora }}</td>
                            <td> {{ $registro->created_at }}</td>
                            <td class="text-right">

                                <form action="{{ route('posts.destroy', $registro->id) }}" method="POST">       
                                   @csrf
                                   @method('delete')
                                   <a href="{{ route('posts.edit', $registro->id) }}" class="btn  btn-sm btn-primary" data-toggle="tooltip" title="Editar">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button type="submit" class="delete btn  btn-sm btn-danger" data-toggle="tooltip" title="Excluir">
                                    <i class="fa fa-trash"></i>
                                </button> 

                            </form>

                        </td>                            
                    </tr>
                    @endforeach  
                </tbody>
            </table>               
        </div>
    </div>

</div>
</div>

@stop

@section('css')

@stop

@section('js')
<script> 
    $(function () {
        $(".dataTable").DataTable({
            "order": [[ 1, "desc" ]],
            "responsive": true,
            "autoWidth": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Portuguese-Brasil.json"
            }
        });
    });

    $("body").on("click", ".delete", function(e) {

        e.preventDefault();
        form = $(this).closest('form');
        botao = $(this);
        Swal.fire({
            title: 'Você deseja excluir o registro?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, desejo excluir!',
            cancelButtonText: 'Não, cancelar!',
        }).then((result) => {
            if (result.value) {
                botao.data('loading-text', '<i class="fa fa-refresh fa-spin"></i> Excluindo...');
                botao.button('loading');
                botao.attr('disabled');
                form.submit();
            }
        })
    });
</script>
@stop