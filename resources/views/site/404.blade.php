@extends('site.tema.base')
@section('conteudo')
<div class="container" style="height: 90%">

					<section class="page-not-found">
						<div class="row justify-content-center">
							<div class="col-lg-7 text-center">
								<div class="page-not-found-main">
									<h2>404 <i class="fas fa-file"></i></h2>
									<p>Essa página que você está procurando não existe</p>
								</div>
							</div>
							<div class="col-lg-4">
								<h4 class="heading-primary">Alguns links úteis</h4>
								<ul class="nav nav-list flex-column">
									<li class="nav-item"><a class="nav-link" href="{{ route('inicio') }}">Início</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ route('posts') }}">Posts</a></li>
									<li class="nav-item"><a class="nav-link" href="{{ route('contato') }}">Contato</a></li>
									
								</ul>
							</div>
						</div>
					</section>

				</div>
@endsection