@section('titulo')
Contato
@endsection

@extends('site.tema.base')
@section('conteudo')
<div class="container">
	<div class="row pt-5">
		<div class="col-lg-7">
			<h1 class="mb-0">Contato</h1>
			<div class="divider divider-primary divider-small mb-4">
				<hr class="mr-auto">
			</div>

			@if (Session::get('success'))
			<div class="alert alert-success mt-4" >
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{{ Session::get('success') }}
			</div>
			@endif

			@if (Session::get('error'))
			<div class="alert alert-danger d-none mt-4">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{{ Session::get('error') }}
			</div>
			@endif


			<form id="contactForm" action="{{ route('enviar-email') }}" method="post" >
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-row">
					<div class="form-group col-lg-6">
						<label>Nome *</label>
						<input type="text" value="" data-msg-required="Digite o seu nome" maxlength="100" class="form-control" name="nome" id="name" required="">
					</div>
					<div class="form-group col-lg-6">
						<label>E-mail *</label>
						<input type="email" value="" data-msg-required="Digite o seu e-mail" data-msg-email="Digite um e-mail válido" maxlength="100" class="form-control" name="email" id="email" required="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label>Assunto *</label>
						<input type="text" value="" data-msg-required="Digite o assunto" maxlength="100" class="form-control" name="assunto" id="subject" required="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label>Mensagem *</label>
						<textarea maxlength="5000" data-msg-required="Digite a mensagem" rows="5" class="form-control" name="mensagem" id="message" required=""></textarea>
					</div>
				</div>
				<input type="hidden" name="captcha" value="" id="captcha">

				<div class="form-row">
					<div class="form-group col">
						<input type="submit" value="Enviar" class="btn btn-primary btn-lg" data-loading-text="Loading...">
					</div>
				</div>
			</form>

		</div>
		<div class="col-lg-4 col-lg-offset-1">
			<h4 class="pt-4 mb-0">Atendimento</h4>
			<div class="divider divider-primary divider-small mb-4">
				<hr class="mr-auto">
			</div>
			<ul class="list list-icons list-dark mt-4">
				<li><i class="fas fa-home"></i> Atendimento domiciliar</li>
				<li><i class="fas fa-user"></i> Atendimento em consultório</li>

				<li><i class="fas fa-desktop"></i> Atendimento online (profissional cadastrado no sistema e-Psi de acordo com a Resolução CFP nº 011/2018)</li>

			</ul>

			<div class="divider divider-primary divider-small mb-4">
				<hr class="mr-auto">
			</div>
			<ul class="list list-icons list-icons-style-3 mt-4 mb-4">

				<li><i class="fab fa-whatsapp"></i> <strong>Telefone:</strong> (51) 9 9684-7135</li>
				<li><i class="far fa-envelope"></i> <strong>Email:</strong> serd.jr@gmail.com </li>
				<li><i class="fas fa-map-marker-alt"></i> <strong>Endereço: </strong>Avenida Independência, número 925, sala 1009, Bairro Independência, Porto Alegre. 
					CEP 90035-076
					</li>
			</ul>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.2517821737574!2d-51.213886485400955!3d-30.029633681887734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951979ae0ec68ecd%3A0xcb89a4dc85449d27!2sAv.%20Independ%C3%AAncia%2C%20925%20-%20Independ%C3%AAncia%2C%20Porto%20Alegre%20-%20RS%2C%2090035-076!5e0!3m2!1spt-BR!2sbr!4v1637670632384!5m2!1spt-BR!2sbr" width="100%" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	</div>
</div>

<script src="https://www.google.com/recaptcha/api.js?render=6Lfe8cEZAAAAAAcBHT1DtTcEp8BcQx5-SYo9CUma"></script>
<script type="text/javascript">
	
	grecaptcha.ready(function() {
		grecaptcha.execute('6Lfe8cEZAAAAAAcBHT1DtTcEp8BcQx5-SYo9CUma').then(function(token) {
			
			if(token){
				
				$('#captcha').val(token);
				return true;
			}
		});
	});

</script>
@endsection