@section('titulo')
    {{ $titulo }}
@endsection

@section('css')
    <link rel="preload" href="{{ asset('css/magnific-popup.css') }}" type="text/css" defer />
@endsection

@extends('site.tema.base')
@section('conteudo')
    <div class="container">
        <div class="row pt-5">

            <div class="col-lg-12" style="font-family: 'Raleway', sans-serif !important;">
                <h1 class="mb-0">{{ $titulo }}
                </h1>
                @if (isset($subtitulo))
                    <div class="post-meta mt-3">
                        <h6>{{ $subtitulo }}</h6>
                    </div>
                @endif
                <div class="divider divider-primary divider-small mb-4">
                    <hr class="mr-auto">
                </div>
                <img width="350" class="img-fluid float-right ml-4 mb-4 mt-1" alt="{{ $titulo }}"
                    src="{{ $imagem }}">

                    {!! $texto !!}
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
@endsection
