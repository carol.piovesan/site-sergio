@section('titulo')
Especialidades
@endsection
@extends('site.tema.base')
@section('conteudo')
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-12">
                {!! $especialidades->texto !!}
            </div>
        </div>
    </div>
@endsection
