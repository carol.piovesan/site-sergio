@section('titulo')
    Início
@endsection

@extends('site.tema.base')
@section('conteudo')

@include('site.partes.banner')
@include('site.partes.destaques')

@if(isset($posts) && $posts->count() > 0)
@include('site.partes.ultimos-posts')
@endif
@endsection

