<div class="slider-container rev_slider_wrapper">
		<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000,  'gridheight': 330, 'disableProgressBar': 'on'}">

			<ul>
				<li data-transition="fade">
					<img src="{{ asset('imagem/site/banner.jpg') }}"
					alt="Imagem do Banner"
					data-bgposition="center center"
					
					data-bgrepeat="no-repeat"
					class="rev-slidebg">
					
					<div class="tp-caption main-label"
					data-x="left" data-hoffset="0"
					data-y="center" data-voffset="0"
					data-start="1500"
					data-width="600"
					data-transform_in="y:[80%];s:500;"
					data-transform_out="opacity:0;s:500;"
					style="z-index: 5"
					data-mask_in="x:0px;y:0px;">A interação entre os processos neurobiológicos e os relacionamentos interpessoais.</div>
				</li>
			</ul>
		</div>
	</div>
