@if(isset($destaques))
<section class="section section-default section-no-border mt-0">
    <div class="container">
        <div class="row mt-4">
            <div class="col-lg-4 ">
                <div class="mb-4">
                    <a href="{{ route('pagina', ['1', $destaques->slug_1]) }}">
                        <img src="{{$destaques->getImagemn1Attribute()}}">
                    </a> 
                    <div class="mt-2 borda">
                        <h4>
                            <a href="{{ route('pagina', ['1',$destaques->slug_1]) }}" class="text-dark">
                               {{$destaques->titulo_1}}
                            </a>
                        </h4>
                        @if(isset($destaques->subtitulo_1))
                        <p class="mb-0">{{$destaques->subtitulo_1}}
                        </p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="mb-4">
                    <a href="{{ route('pagina', ['2',$destaques->slug_2]) }}">
                        <img src="{{$destaques->getImagemn2Attribute()}}">
                    </a>
                    <div class="mt-2 borda">
                        <h4>
                            <a href="{{ route('pagina', ['2',$destaques->slug_2]) }}" class="text-dark">
                                {{$destaques->titulo_2}}
                             </a>
                        </h4>
                        <p class="mb-0">{{$destaques->subtitulo_2}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="mb-4">
                    <a href="{{ route('pagina', ['3',$destaques->slug_3]) }}">
                        <img src="{{$destaques->getImagemn3Attribute()}}">
                    </a>
                    <div class="mt-2 borda">
                        <h4>
                            <a href="{{ route('pagina', ['3',$destaques->slug_3]) }}" class="text-dark">
                                {{$destaques->titulo_3}}
                             </a>
                        </h4>
                        <p class="mb-0">{{$destaques->subtitulo_3}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="mb-4">
                    <a href="{{ route('pagina', ['4',$destaques->slug_4]) }}">
                        <img src="{{$destaques->getImagemn4Attribute()}}">
                    </a> 
                    <div class="mt-2 borda">
                        <h4>
                            <a href="{{ route('pagina', ['4',$destaques->slug_4]) }}" class="text-dark">
                               {{$destaques->titulo_4}}
                            </a>
                        </h4>
                        @if(isset($destaques->subtitulo_4))
                        <p class="mb-0">{{$destaques->subtitulo_4}}
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

</section>
@endif
