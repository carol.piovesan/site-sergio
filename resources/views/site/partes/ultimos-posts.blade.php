<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<h2 class="mt-4 mb-0">Últimos Posts</h2>
			<div class="divider divider-primary divider-small divider-small-center mb-4">
				<hr>
			</div>
		</div>
	</div>
	<div class="row">
		@foreach($posts as $post)

		<div class="col-lg-4">
			<span class=" mb-5">
			
					<a title="" href="{{ route('posts.postagem', $post->slug) }}">
						<img src="{{ $post->imagem}}" class="img-fluid" alt="" >
					</a>
				
				<span class="thumb-info-caption">
					<span class="thumb-info-caption-text">
						<h4 class="mb-3 mt-1"><a title="" class="titulo-post" href="{{ route('posts.postagem', $post->slug) }}">{{$post->titulo}}</a></h4>
						<span class="post-meta">
							<span>
								{{$post->data_hora_extenso}}
							</span>
							<p class="post-texto">

								{!! $post->parteDescricao !!}
							</p>
							<a class="mt-3" href="{{ route('posts.postagem', $post->slug) }}">Leia Mais <i class="fas fa-long-arrow-alt-right"></i>
							</a>
						</span>
					</span>
				</span>
			</span>

		</div>
		@endforeach

	</div>
</div>