@section('titulo')
{{$post->titulo}}
@endsection
@extends('site.tema.base')
@section('conteudo')
<div class="container">
	<div class="row pt-5">
		<div class="col">
			<div class="blog-posts single-post">
				<article class="post blog-single-post">
					<div class="post-content">
						<h1>{{$post->titulo}}</h1>
						<div class="divider divider-primary divider-small mb-4">
							<hr class="mr-auto">
						</div>
						<div class="post-meta">
							<span>{{$post->data_hora_extenso}}</span>
						</div>

						@if($post->capa)
						<img src="{{$post->imagem}}" class="img-fluid float-left mb-1 mt-2 mr-4" alt="" >
						@endif
						
						{!! $post->texto !!}
					</div>
				</article>
			</div>
		</div>
	</div>
</div>
@endsection