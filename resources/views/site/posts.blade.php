@section('titulo')
Posts
@endsection
@extends('site.tema.base')
@section('conteudo')
<div class="container">
	<div class="row pt-5 pb-4">

		<div class="col">

			<h1 class="mb-0">Posts</h1>
			<div class="divider divider-primary divider-small mb-4">
				<hr class="mr-auto">
			</div>

			@foreach($posts as $post)
			<div class="row mt-4">
				<div class="col">

					<span class="thumb-info thumb-info-side-image thumb-info-no-zoom mt-4">
						<span class="thumb-info-side-image-wrapper p-0 d-none d-md-block">
							<a title="" href="{{ route('posts.postagem', $post->slug) }}">
								<img src="{{$post->imagem}}" class="img-fluid" alt="" >
							</a>
						</span>
						<span class="thumb-info-caption">
							<span class="thumb-info-caption-text">
								<h2 class="mb-3 mt-1"><a title="" class="text-dark" href="{{ route('posts.postagem', $post->slug) }}">{{$post->titulo}}</a></h2>
								<span class="post-meta">
									<span>{{$post->data_hora_extenso}}</span>
								</span>
								<p class="text-3 px-0 px-md-3">{!! $post->parteDescricaoMaior !!}</p>
								<a class="mt-3" href="{{ route('posts.postagem', $post->slug) }}">Leia Mais <i class="fas fa-long-arrow-alt-right"></i></a>
							</span>
						</span>
					</span>

				</div>
			</div>
			@endforeach
			<div class="row mt-4">
				<div class="col-sm-12">
					<nav class="text-center">
						{{ $posts->appends(Request::all())->render() }}
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection