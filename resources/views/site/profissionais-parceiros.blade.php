@section('titulo')
Profissionais Parceiros
@endsection

@extends('site.tema.base')
@section('conteudo')
<div role="main" class="main">
	<div class="container">
		<div class="row pt-5">

			<div class="col-lg-12">

				<h1 class="mb-0">Profissionais parceiros</h1>
				<div class="divider divider-primary divider-small mb-4">
					<hr class="mr-auto">
				</div>

				<div class="row team-list mt-1 sort-destination">
					<div class="col-md-6 offset-lg-1 col-lg-3  mb-5 text-center isotope-item criminal-law new-york">
						<a href="#" data-toggle="modal" data-target="#juliana">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="{{ asset('imagem/site/profissionais-parceiros/juliana.jpeg')}}" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Juliana Miranda</h4>
						<p class="mb-0">Psicóloga CRP 07/28483</p>
						<p>
							Psicóloga (UFRGS)<br>
							Especialista em Terapias Cognitivas (Wainer - Psicologia Cognitiva)	
						</p>

					</div>

					<div class="modal fade" id="juliana" tabindex="-1" role="dialog" aria-labelledby="julianaLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="julianaLabel">Juliana Miranda - Psicóloga CRP 07/28483</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										Psicóloga (UFRGS)<br>
										Especialista em Terapias Cognitivas (Wainer - Psicologia Cognitiva)
										<br>
										Terapia Cognitivo-Comportamental com adolescentes, adultos e idosos. <br>
										Intervenção Neuropsicológica com adultos e idosos. <br>
										Atendimento presencial e online.<br>
										Contato: (51) 99968-1196 / julianarmirand@hotmail.com<br>
										Endereço: Porto Alegre, Av. Plínio Brasil Milano, 289, sala 102.
									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-3 offset-lg-1 mb-5 text-center isotope-item business-law new-york">
						<a href="#" data-toggle="modal" data-target="#giovanna">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="{{ asset('imagem/site/profissionais-parceiros/giovanna.jpg')}}" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Giovanna Piccoli</h4>
						<p class="mb-0">Psicóloga CRP 07/27027</p>
						<p>Especialista em Terapias Cognitivo-Comportamentais
<br>
Formação em Terapia do Esquema (Wainer -Psicologia Cognitiva)
						</p>
					</div>
					<div class="modal fade" id="giovanna" tabindex="-1" role="dialog" aria-labelledby="giovannaLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="giovannaLabel">Giovanna Piccoli - Psicóloga CRP 07/27027</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										Especialista em Terapias Cognitivo-Comportamentais pelo Instituto de Terapias Cognitivo-Comportamentais (InTCC) – Porto Alegre/RS<br>
										Formação em Terapia do Esquema pela Wainer Psicologia Cognitiva credenciada pelo New Jersey Institute for Schema Therapy - NJ/NYC. 
										<br>
										Profissional associada a Associação  Brasileira de Terapia do Esquema (ABTE)
										<br>
										Atua nas áreas de avaliação  neuropsicólogica, reabilitação  neuropsicóloga e terapia cognitivo-comportamental - ênfase em terapia do esquema.
										  <br>
										Endereço: Avenida Goethe, 21/ Sala 405<br>
										Contato:  (51)998224590/ giovannalpiccoli@gmail.com

									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-3 offset-lg-1 mb-5 text-center isotope-item business-law new-york">
						<a href="#" data-toggle="modal" data-target="#helena">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="{{ asset('imagem/site/profissionais-parceiros/helena.jpeg')}}" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Helena Santos Guido</h4>
						<p class="mb-0">Psicóloga CRP 07/31531</p>
						<p>Especializanda em Psicologia do Desenvolvimento e Aprendizagem</p>
					</div>
					<div class="modal fade" id="helena" tabindex="-1" role="dialog" aria-labelledby="helenaLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="helenaLabel">Helena Santos Guido - Psicóloga CRP 07/31531</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										Especializanda em Psicologia do Desenvolvimento e Aprendizagem<br>
										Psicoterapia, Avaliação e Reabilitação Neuropsicológica e Consultoria em Psicologia Escolar. 
										<br>Atendimento presencial e online.  <br>
										Endereço: Av Ijui, 53- sala 503. Bairro Petrópolis- Porto Alegre, RS<br>
										Contato: 997235609/ psicohelenaguido@hotmail.com

									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>


					<div class="col-md-6 col-lg-3 offset-lg-3 mb-5 text-center isotope-item divorce-law london">
						<a href="#" data-toggle="modal" data-target="#tiago">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="{{ asset('imagem/site/profissionais-parceiros/tiago.jpeg')}}" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Tiago Canto</h4>
						<p class="mb-0">Psicólogo CRP 07/31535</p>
						<p>
							Psicólogo Clínico e Esportivo, Bacharel em Comunicação Social. 
							<br>Especializando em Neuropsicologia.
							<br> Especializando em Treinamento Desportivo e Especializando em Educação Especial Inclusiva.
						</p>
					</div>
					<div class="modal fade" id="tiago" tabindex="-1" role="dialog" aria-labelledby="helenaLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="helenaLabel">Tiago Canto -
									Psicólogo CRP 07/31535</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										Psicólogo Clínico e Esportivo, Bacharel em Comunicação Social. 
										<br>Especializando em Neuropsicologia.
										<br> Especializando em Treinamento Desportivo e Especializando em Educação Especial Inclusiva.<br>

										
										Psicoterapia Adulto e idoso, Abordagem Cognitivo-Comportamental.<br> Atendimento Online.<br> Reabilitação Através do Esporte.<br>
										Contato: cantotiago@gmail.com

									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 offset-lg-1 mb-5 text-center isotope-item accident-law london">
						<a href="#" data-toggle="modal" data-target="#renata">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="{{ asset('imagem/site/profissionais-parceiros/renata.jpeg')}}" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Renata Giuliani Endres</h4>
						<p class="mb-0">Psicóloga CRP 07/14213</p>
						<p>Psicóloga (PUCRS), Doutora em Psicologia (UFRGS), Mestre em Psicologia do Desenvolvimento (UFRGS), Especialista em Transtornos do Desenvolvimento (UFRGS); cursando Especialização em Terapia Cognitivo-Comportamental
						</p>
						
					</div>

					<div class="modal fade" id="renata" tabindex="-1" role="dialog" aria-labelledby="renataLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="helenaLabel">Renata Giuliani Endres - Psicóloga CRP 07/14213</h4>
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body">
									<p>
										Titulação - Psicóloga (PUCRS), Doutora em Psicologia (UFRGS), Mestre em Psicologia do Desenvolvimento (UFRGS), Especialista em Transtornos do Desenvolvimento (UFRGS); cursando Especialização em Terapia Cognitivo-Comportamental <br>
										Área de atuação Avaliação e intervenção em Transtorno do Espectro Autista (TEA) voltada para indivíduos adultos; <br>
										Psicologia clínica na abordagem das Terapias Cognitivo-comportamentais. <br>
										Endereço:  Rua Fernandes Vieira, 325, sala 301 - Bom Fim - Porto Alegre <br>
										Contato rgendres01@gmail.com (51) 98049-4807


									</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
								</div>
							</div>
						</div>
					</div>
					<!--
					<div class="col-md-6 col-lg-3 mb-5 text-center isotope-item health-law new-york">
						<a href="demo-law-firm-attorneys-detail.html">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="img/team/team-29.jpg" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Amanda Doe</h4>
						<p class="mb-0">Health Law</p>
						<span class="thumb-info-social-icons mt-2 pb-0">
							<a href="http://www.facebook.com" target="_blank"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
							<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
							<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
						</p>
					</div>
					<div class="col-md-6 col-lg-3 mb-5 text-center isotope-item capital-law new-york">
						<a href="demo-law-firm-attorneys-detail.html">
							<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
								<span class="thumb-info-wrapper">
									<img src="img/team/team-30.jpg" class="img-fluid" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Saiba mais</span>
									</span>
								</span>
							</span>
						</a>
						<h4 class="mt-3 mb-0">Jessica Doe</h4>
						<p class="mb-0">Capital Law</p>
						<span class="thumb-info-social-icons mt-2 pb-0">
							<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
						</p>
					</div>
				-->
			</div>

		</div>
	</span>
</div>
</span>
@endsection
