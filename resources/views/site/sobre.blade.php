@section('titulo')
Conheça o Profissional
@endsection

@extends('site.tema.base')
@section('conteudo')
    <div class="container">
        <div class="row pt-5">
            <div class="col-lg-12">
                <h1 class="mb-0">Sobre</h1>
                <div class="divider divider-primary divider-small mb-4">
                    <hr class="mr-auto">
                </div>
                <b>O perfil profissional
                </b>
                <p class="mt-4">
                    Na prática clínica realizo acompanhamento psicoterápico considerando a influência dos vínculos afetivos na construção do psiquismo, nas vivências emocionais e seus efeitos nas formas de comunicação e interação com outras pessoas. Considero, em especial, as particularidades dos vínculos formados e modificados em situações de alterações cognitivas em diferentes contextos e fases do ciclo vital individual.
                </p>
                <p class="mt-4">
                    Na prática acadêmica atual dedico-me à pesquisa de métodos de acesso e avaliação de fenômenos cognitivos. Especificamente, estudo a forma como o pensamento e a linguagem são articulados a partir do contexto e da interação social.
                </p>
                <b>A especialidade
                </b>
                <p class="mt-4">A especialidade profissional foi construída naturalmente ao longo de mais de uma década de prática clínica, nos contextos da saúde privada e da saúde pública. No exercício profissional na área da neuropsicologia, onde trabalhei com os procedimentos de avaliação e de reabilitação neuropsicológica, tive um contato muito próximo com crianças, adolescentes, adultos e idosos com alterações cognitivas variando desde as alterações mais leves e sutis até as mais invasivas, desorganizadas ou incapacitantes. Invariavelmente durante a vivência clínica acabei me aproximando das famílias dessas pessoas com dificuldades e pude entender como um transtorno do neurodesenvolvimento, uma lesão neurológica adquirida, uma má-formação neurológica ou alguma doença neurodegenerativa (demência), por exemplo, afeta a família como um todo e de formas diferentes. Os vínculos afetivos estabelecidos antes da doença ser identificada são modificados, os filhos acabam tendo que cuidar dos próprios pais, os pais precisam ajustar as expectativas quando ocorrem alterações no desenvolvimento de seus filhos, conflitos familiares antigos que acabam aparecendo, novos conflitos familiares que acabam tomando forma, também faziam parte da minha rotina como neuropsicólogo. Naturalmente acabei vendo a minha prática clínica e o meu interesse pessoal serem direcionados para o manejo das transformações dos vínculos afetivos. A partir desse momento acabei me dedicando aos processos emocionais comumente encontrados ao longo do ciclo vital, tanto no contexto da neuropsicologia como no contexto de pessoas com o desenvolvimento típico (sem alterações neuropsicológicas).
                </p>
                <p class="mt-4">No cerne da Psicologia do Desenvolvimento, a Teoria do Apego, elaborada pelo psiquiatra John Bowlby (1969; 1973; 1980) e pela psicóloga Mary Ainsworth (1967), mostra-se como um recurso clínico teórico e técnico capaz de unificar os processos determinantes para o desenvolvimento humano. A Teoria do Apego considera a importância dos fenômenos biológicos que são próprios da espécie (filogênese) e particulares de cada pessoa (ontogênese), em um sistema comportamental que interage diretamente com o desenvolvimento físico, emocional, cognitivo e social.
                </p>
            </div>
           
        </div>
    </div>
@endsection
