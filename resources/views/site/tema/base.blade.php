<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>@yield('titulo') . Psicologia Clínica | Sergio Duarte Junior</title>	

	<meta name="keywords" content="psicologia clínica, psicologia, psicologo, psicologia, neuropsicologo, porto alegre, avaliação neuropsicológica, reabilitação neuropsicológica, sergio, sergio duarte junior" />

	<meta name="description" content="Psicologia Clínica | Sergio Duarte Junior - Psicólogo CRP 0720911">
	<meta name="author" content="https://www.instagram.com/caroolpm/?hl=pt-br">

	<link rel="shortcut icon" href="{{ asset('imagem/site/fav.png') }}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	
	<!-- Estilo -->
	@yield('css')

	<link rel="preload" href="{{ asset('/webfonts/fa-solid-900.woff2')}}" as="font" type="font/woff2" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/site.css')}}">
	
</head>
<body>
	<div class="body">
		@include('site.tema.cabecalho')
		<section id="content">
			<div role="main" class="main">
				@yield('conteudo')
			</div>
		</section>
		<script src="{{ asset('/js/site.js')}}"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var a = window.location;
				$('#primary-menu > ul > li > a[href="' + a + '"]').addClass("active"), $("#primary-menu > ul > li > a").filter(function() {
					return this.href == a
				}).addClass("active");

				var mFoo = $("#footer");
                if ((($(document.body).height() + mFoo.outerHeight()) < $(window).height() && mFoo.css("position") == "fixed") || ($(document.body).height() < $(window).height() && mFoo.css("position") != "fixed")) {
                mFoo.css({ position: "fixed", bottom: "0px" ,width: '100%'});
                } else {
                mFoo.css({ position: "static" });
                }

			}); 
			
			 
		</script>


	</div>
	@include('site.tema.rodape')
</body>
@yield('js')
</html>