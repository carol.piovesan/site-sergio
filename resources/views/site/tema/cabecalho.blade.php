<header id="header" class="header-narrow"
data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 88, 'stickySetTop': '0px', 'stickyChangeLogo': false}">
<div class="header-body">
	<div class="header-container container">
		<div class="header-row">
			<div class="header-column">
				<div class="header-row">
					<div class="header-logo">
						<a href="{{ route('inicio') }}">
							{{-- <img alt="Porto" src="{{ asset('imagem/site/logo.png') }}"> --}}
							<span>
								<h2 class="no">
									Sergio Duarte Junior
								</h2>
								<h4>Psicólogo Clínico CRPRS 0720911</h4>
							</span> </a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-top-line">
							<div
							class="header-nav-main header-nav-main-no-arrows header-nav-main-square header-nav-main-effect-3 header-nav-main-sub-effect-1">
							<nav class="collapse" id="primary-menu">
								<ul class="nav nav-pills" id="mainNav">
									<li>
										<a class="nav-link" href="{{ route('inicio') }}">
											Início
										</a>
									</li>
									<li class="dropdown">
										<a class="dropdown-item dropdown-toggle {{ strpos(Route::currentRouteName(), 'sobre') !== false || strpos(Route::currentRouteName(), 'profissionais-parceiros') !== false ? 'active' : null }} "
										href="#">
										Sobre
										<i class="fas fa-caret-down"></i>
									</a>

									<ul class="dropdown-menu">
										<li>
											<a class="dropdown-item" href="{{ route('sobre') }}">O perfil profissional</a>
										</li>
										<li>
											<a class="dropdown-item"
											href="{{ route('profissionais-parceiros') }}">Profissionais
										Parceiros</a>
									</li>
								</ul>
							</li>

							@if(isset($destaques))
							<li class="dropdown">
								<a class="dropdown-item dropdown-toggle {{ strpos(Route::currentRouteName(), 'pagina') !== false ? 'active' : null  }} " href="#">
									Tópicos
									<i class="fas fa-caret-down"></i>
								</a> 
								<ul class="dropdown-menu">
									<li>
										<a href="{{ route('pagina', ['1',$destaques->slug_1]) }}" class="dropdown-item">
											{{$destaques->titulo_1}}
										</a>
									</li>
									<li>
										<a class="dropdown-item" href="{{ route('pagina', ['2',$destaques->slug_2]) }}">
										{{$destaques->titulo_2}}
										</a>
									</li>
									<li>
										<a href="{{ route('pagina', ['3',$destaques->slug_3]) }}" class="dropdown-item">
											{{$destaques->titulo_3}}
										</a>
									</li>
									<li>
										<a href="{{ route('pagina', ['4',$destaques->slug_4]) }}" class="dropdown-item">
											{{$destaques->titulo_4}}
										</a>
									</li>
								</ul>
							</li>
							@endif
						

						{{-- <li class="dropdown">
							<a class="dropdown-item dropdown-toggle {{ strpos(Route::currentRouteName(), 'pagina') !== false ? 'active' : null  }} " href="#">
								Neuropsicologia Clínica
								<i class="fas fa-caret-down"></i>
							</a>

							<ul class="dropdown-menu ">

								<li class="dropdown-submenu dropdown-reverse">
									<a class="dropdown-item" href="#">Procedimentos oferecidos<i class="fas fa-caret-down"></i></a>
									<ul class="dropdown-menu">
										<li><a class="dropdown-item" href="{{ route('pagina', 'avaliacao-neuropsicologica') }}">Avaliação Neuropsicológica</a></li>

										<li><a class="dropdown-item" href="{{ route('pagina', 'programas-intervencao-neuropsicologica') }}">Programas de Intervenção  Neuropsicológica</a></li>
									</ul>
								</li>
								<li class="dropdown-submenu dropdown-reverse">
									<a class="dropdown-item" href="#">Contexto Profissional<i class="fas fa-caret-down"></i></a>
									<ul class="dropdown-menu">
										<li><a class="dropdown-item" href="{{ route('pagina', 'neuropsicologia-clinica-aplicada-ao-neurodesenvolvimento') }}">Neuropsicologia clínica aplicada ao neurodesenvolvimento</a></li>

										<li><a class="dropdown-item" href="{{ route('pagina', 'modelos-conceptuais-da-intervencao-neuropsicologica-na-infancia') }}">Modelos conceptuais da intervenção neuropsicológica na infância</a></li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'neuropsicologia-do-desenvolvimento-e-linguagem') }}">
												Neuropsicologia do desenvolvimento e linguagem
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'envelhecimento-bem-sucedido') }}">
												Envelhecimento bem sucedido
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'modelos-neurolinguisticos-nas-demencias-corticais-e-subcorticais') }}">
												Modelos neurolinguísticos nas demências corticais e subcorticais
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'neuropsicologia-da-linguagem-demencias-tipo-alzheimer-e-nao-alzheimer') }}">
												Neuropsicologia da linguagem: demências tipo alzheimer e não alzheimer
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'reabilitacao-das-alteracoes-de-compreensao-da-linguagem-escrita') }}">
												Reabilitação neuropsicológica das alterações de compreensão e escrita
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'reabilitacao-da-linguagem-baseada-na-semiologia-dos-disturbios-linguisticos') }}">
												Reabilitação neuropsicológica baseada na semiologia dos distúrbios linguísticos
											</a>
										</li>
										<li>
											<a class="dropdown-item" href="{{ route('pagina', 'reabilitacao-dos-aspectos-pragmaticos-da-linguagem') }}">
												Estimulação neuropsicológica dos aspectos pragmáticos da linguagem
											</a>
										</li>
									</ul>
								</li>
								<li class="dropdown-submenu dropdown-reverse">
									<a class="dropdown-item" href="#">Projetos<i class="fas fa-caret-down"></i></a>
									<ul class="dropdown-menu">
										<li><a class="dropdown-item" href="{{ route('pagina', 'conte-comigo') }}">Projeto Conte Comigo!</a></li>

										<li><a class="dropdown-item" href="{{ route('pagina', 'saiba-mais') }}">Projeto Saiba Mais!</a></li>
										<li><a class="dropdown-item" href="{{ route('pagina', 'vamos-juntos') }}">Projeto Vamos Juntos!</a></li>
									</ul>
								</li>
							</ul>
						</li> --}}
						<li>
							<a class="nav-link  {{ strpos(Route::currentRouteName(), 'especialidades') !== false ? 'active' : null }} "
							href="{{ route('especialidades') }}">
							Especialidades
						</a>
					</li>

					<li>
						<a class="nav-link  {{ strpos(Route::currentRouteName(), 'posts') !== false ? 'active' : null }} "
						href="{{ route('posts') }}">
						Posts
					</a>
				</li>

				<li>
					<a class="nav-link" href="{{ route('contato') }}">
						Contato
					</a>
				</li>

			</ul>
		</nav>
	</div>

	<button class="btn header-btn-collapse-nav" data-toggle="collapse"
	data-target=".header-nav-main nav">
	<i class="fas fa-bars"></i>
</button>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
