<footer class="short" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p>© Copyright 2020 - Neuropsicologia clínica <span class="text-color-light">Sergio Duarte Junior - Psicólogo CRPRS 0720911 - (51) 9 9684-7135</span> - Todos os direitos reservados</p>
				<br>
				<p>DESENVOLVIDO POR  <a href="https://www.instagram.com/caroolpm/?hl=pt-br" target="_blank">CAROLINE PIOVESAN</a> </p>
			</div>
		</div>
	</div>
</footer>