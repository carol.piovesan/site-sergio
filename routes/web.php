<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('inicio');
Route::get('/pg/{destaque}/{pagina}', 'SiteController@pagina')->name('pagina');
Route::get('/sobre', 'SiteController@sobre')->name('sobre');
Route::get('/profissionais-parceiros', 'SiteController@profissionaisParceiros')->name('profissionais-parceiros');
Route::get('/posts', 'SiteController@posts')->name('posts');
Route::get('/especialidades', 'SiteController@especialidades')->name('especialidades');
Route::get('/posts/{slug}', 'SiteController@postagem')->name('posts.postagem');
Route::get('/contato', 'SiteController@contato')->name('contato');
Route::post('/enviar-email', 'SiteController@enviarEmail')->name('enviar-email');

Route::get('erro', function(){
	return view('site/404');
})->name('erro');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::resource('admin/posts', 'PostsController');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::resource('admin/especialidades', 'EspecialidadesController');


Auth::routes();
Route::resource('admin/destaques', 'DestaquesController');
