const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.styles([
 	'resources/sass/bootstrap.min.css',
 	'resources/sass/fontawesome-all.min.css',
 	'resources/sass/animate.min.css',
 	'resources/sass/simple-line-icons.min.css',
 	'resources/sass/owl.carousel.min.css',
 	'resources/sass/owl.theme.default.min.css',
 	'resources/sass/magnific-popup.min.css',
 	'resources/sass/theme.css',
 	'resources/sass/theme-elements.css',
 	'resources/sass/theme-blog.css',
 	'resources/sass/theme-shop.css',
 	'resources/sass/layers.css',
 	'resources/sass/demo-law-firm.css',
 	'resources/sass/skin-law-firm.css',
 	'resources/sass/font-raleway.css',
 	'resources/sass/custom.css',
 	], 'public/css/site.css');


 mix.scripts([
 	'resources/js/jquery.min.js',
 	'resources/js/jquery.appear.min.js',
 	'resources/js/jquery.easing.min.js',
 	'resources/js/jquery-cookie.min.js',
 	'resources/js/popper.min.js',
 	'resources/js/bootstrap.min.js',
 	'resources/js/common.min.js',
 	'resources/js/jquery.validation.min.js',
 	'resources/js/jquery.easy-pie-chart.min.js',
 	'resources/js/jquery.gmap.min.js',
 	'resources/js/jquery.lazyload.min.js',
 	'resources/js/jquery.isotope.min.js',
 	'resources/js/owl.carousel.min.js',
 	'resources/js/jquery.magnific-popup.min.js',
 	'resources/js/vide.min.js',
 	'resources/js/theme.js',
 	'resources/js/jquery.themepunch.tools.min.js',
 	'resources/js/jquery.themepunch.revolution.min.js',
 	'resources/js/view.contact.js',
 	'resources/js/demo-law-firm.js',	
 	'resources/js/custom.js',
 	'resources/js/theme.init.js'
 	], 'public/js/site.js');


 mix.js(
 	'resources/js/app.js', 'public/js')
 .sass('resources/sass/app.scss', 'public/css');
